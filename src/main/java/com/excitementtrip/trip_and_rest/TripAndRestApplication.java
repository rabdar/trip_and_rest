package com.excitementtrip.trip_and_rest;

import com.excitementtrip.trip_and_rest.testData.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TripAndRestApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(TripAndRestApplication.class, args);

        UserTestData userTestData = context.getBean(UserTestData.class);
        userTestData.addUserData();

        TestData testData = context.getBean(TestData.class);
        testData.addTestData();

    }
}
