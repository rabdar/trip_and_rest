package com.excitementtrip.trip_and_rest;

import com.excitementtrip.trip_and_rest.entities.User;
import com.excitementtrip.trip_and_rest.repositories.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;


@Controller
public class WelcomeScreenController {

    @Resource(name = "userRepository")
    private UserRepository userRepository;


    @GetMapping
    public String home(){
        return "welcomeScreen";
    }

    @PostMapping("/buyTour")
    public String buyTour(String userName, String email) {
        User user = new User();
        user.setUserName(userName);
        user.setEmail(email);
        userRepository.save(user);
        return "buyTour";
    }


}
