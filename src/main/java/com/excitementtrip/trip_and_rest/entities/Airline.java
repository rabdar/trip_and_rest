package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;
import static org.hibernate.annotations.CascadeType.PERSIST;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "airlines")
public class Airline {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String airlineName;

    @ManyToMany (mappedBy = "airline")
    @Cascade(PERSIST)
    private List<Airport> airport= new ArrayList<>();


    @OneToMany (mappedBy = "airline")
    private List<Tour> tour = new ArrayList<>();

    public Airline(String airlineName) {
        this.airlineName = airlineName;
    }
}
