package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;
import static org.hibernate.annotations.CascadeType.PERSIST;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "airports")
public class Airport {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String airportName;

    private String planeNumber;

    @ManyToMany
    private List<Airline> airline = new ArrayList<>();

    @ManyToMany (mappedBy = "airport")
    @Cascade(PERSIST)
    private List<City> city = new ArrayList<>();

    public Airport(String airportName, String planeNumber) {
        this.airportName = airportName;
        this.planeNumber = planeNumber;
    }
}
