package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;
import static org.hibernate.annotations.CascadeType.PERSIST;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "buy_tours")
public class BuyTour {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private Double price;

    @ManyToOne
    @Cascade(PERSIST)
    private Tour tour;

    @ManyToOne
    @Cascade(PERSIST)
    private User user;

}
