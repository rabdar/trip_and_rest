package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.*;
import static org.hibernate.annotations.CascadeType.PERSIST;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "cities")
public class City {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;
    
    private String cityName;

    @ManyToMany
    private List<Airport> airport = new ArrayList<>();

    @ManyToOne
    @Cascade(PERSIST)
    private Country country;


    @OneToMany(mappedBy = "city")
    private List<Hotel> hotel = new ArrayList<>();

    public City(String cityName) {
        this.cityName = cityName;
    }

}
