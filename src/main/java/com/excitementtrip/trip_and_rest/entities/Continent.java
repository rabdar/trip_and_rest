package com.excitementtrip.trip_and_rest.entities;


public enum Continent {

    AFRICA,
    ANTARCTICA,
    ASIA ,
    AUSTRALIA ,
    EUROPE ,
    NORTH_AMERICA,
    SOUTH_AMERICA,
    ;
}
