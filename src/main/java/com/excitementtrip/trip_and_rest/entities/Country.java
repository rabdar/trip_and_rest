package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table (name = "countries")
public class Country {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String countryName;

    @Enumerated(EnumType.STRING)
    private Continent continent;

    @OneToMany (mappedBy = "country")
    private List<City> city= new ArrayList<>();


    public Country(String countryName, Continent continent) {
        this.countryName = countryName;
        this.continent = continent;
    }
}
