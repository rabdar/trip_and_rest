package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;
import static org.hibernate.annotations.CascadeType.PERSIST;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table (name = "hotels")
public class Hotel {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String hotelName;

    private Integer stars;

    private String description;

    @ManyToOne
    @Cascade(PERSIST)
    private City city;

    @OneToMany (mappedBy = "hotel")
    private List<Tour> tour = new ArrayList<>();

    public Hotel(String hotelName, Integer stars, String description) {
        this.hotelName = hotelName;
        this.stars = stars;
        this.description = description;
    }
}
