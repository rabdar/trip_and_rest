package com.excitementtrip.trip_and_rest.entities;

public enum HotelBoard {
    BB,
    HB,
    FB,
    AI,
}
