package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.springframework.format.datetime.DateFormatter;

import javax.persistence.*;


import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static javax.persistence.GenerationType.IDENTITY;
import static org.hibernate.annotations.CascadeType.PERSIST;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tours")
public class Tour {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @Temporal(TemporalType.DATE)
    private Date departure;

    @Temporal(TemporalType.DATE)
    private Date arrival;

    private Integer daysOfStay;

    @Enumerated(EnumType.STRING)
    private HotelBoard hotelBoard;

    private Double adultPrice;

    private Double childPrice;

    private Boolean promote;

    private Integer numberOfAdults;

    private Integer numberOfChild;


    @ManyToOne
    @Cascade(PERSIST)
    private Hotel hotel;

    @ManyToOne
    @Cascade(PERSIST)
    private Airline airline;

    @OneToMany (mappedBy = "tour")
    private List<BuyTour> buyTour = new ArrayList<>();

    public Tour(String departure, String arrival, Integer daysOfStay, HotelBoard hotelBoard, Double adultPrice,
                Double childPrice, Boolean promote, Integer numberOfAdults, Integer numberOfChild) {

        DateFormatter dateFormatter = new DateFormatter("yyyy-MM-dd");
        try {
            Date dep = dateFormatter.parse(departure, Locale.getDefault());
            Date arr = dateFormatter.parse(arrival, Locale.getDefault());
            this.departure = dep;
            this.arrival = arr;
        } catch (ParseException e) {
            System.out.println("Niepoprawny format daty");
        }
        this.daysOfStay = daysOfStay;
        this.hotelBoard = hotelBoard;
        this.adultPrice = adultPrice;
        this.childPrice = childPrice;
        this.promote = promote;
        this.numberOfAdults = numberOfAdults;
        this.numberOfChild = numberOfChild;
    }
}
