package com.excitementtrip.trip_and_rest.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue (strategy = IDENTITY)
    private Integer id;

    private String userName;

    private String email;

    @OneToMany (mappedBy = "user")
    private List<BuyTour> buyTour;

}
