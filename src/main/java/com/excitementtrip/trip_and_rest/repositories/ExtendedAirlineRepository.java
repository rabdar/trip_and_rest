package com.excitementtrip.trip_and_rest.repositories;

import com.excitementtrip.trip_and_rest.entities.Airline;
import com.excitementtrip.trip_and_rest.repositories.custom.AirlineRepository;
import org.springframework.data.repository.CrudRepository;
import javax.annotation.Resource;

@Resource(name = "extendedAirlineRepository")
public interface ExtendedAirlineRepository extends CrudRepository<Airline, Integer>, AirlineRepository {
}
