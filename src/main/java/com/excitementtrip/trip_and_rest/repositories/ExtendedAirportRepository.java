package com.excitementtrip.trip_and_rest.repositories;


import com.excitementtrip.trip_and_rest.entities.Airport;
import com.excitementtrip.trip_and_rest.repositories.custom.AirportRepository;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.Resource;

@Resource (name = "extendedAirportRepository")
public interface ExtendedAirportRepository extends CrudRepository<Airport, Integer>, AirportRepository {
}
