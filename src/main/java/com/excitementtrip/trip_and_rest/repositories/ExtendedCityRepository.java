package com.excitementtrip.trip_and_rest.repositories;

import com.excitementtrip.trip_and_rest.entities.City;
import com.excitementtrip.trip_and_rest.repositories.custom.CityRepository;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.Resource;

@Resource(name = "extendedCityRepository")
public interface ExtendedCityRepository extends CrudRepository<City, Integer> , CityRepository {
}
