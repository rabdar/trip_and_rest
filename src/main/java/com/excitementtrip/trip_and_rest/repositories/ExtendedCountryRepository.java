package com.excitementtrip.trip_and_rest.repositories;

import com.excitementtrip.trip_and_rest.entities.Country;
import com.excitementtrip.trip_and_rest.repositories.custom.CountryRepository;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.Resource;

@Resource(name = "extendedCountryRepository")
public interface ExtendedCountryRepository extends CrudRepository<Country, Integer>, CountryRepository {
}
