package com.excitementtrip.trip_and_rest.repositories;

import com.excitementtrip.trip_and_rest.entities.Hotel;
import com.excitementtrip.trip_and_rest.repositories.custom.HotelRepository;
import org.springframework.data.repository.CrudRepository;
import javax.annotation.Resource;

@Resource(name = "extendedHotelRepository")
public interface ExtendedHotelRepository extends CrudRepository<Hotel, Integer>, HotelRepository {
}
