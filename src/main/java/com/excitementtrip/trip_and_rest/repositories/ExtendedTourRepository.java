package com.excitementtrip.trip_and_rest.repositories;

import com.excitementtrip.trip_and_rest.entities.Tour;
import com.excitementtrip.trip_and_rest.repositories.custom.TourRepository;
import org.springframework.data.repository.CrudRepository;
import javax.annotation.Resource;

@Resource(name = "extendedTourRepository")
public interface ExtendedTourRepository extends CrudRepository<Tour, Integer>, TourRepository {
}
