package com.excitementtrip.trip_and_rest.repositories;

import com.excitementtrip.trip_and_rest.entities.User;
import org.springframework.data.repository.CrudRepository;
import javax.annotation.Resource;

@Resource(name = "userRepository")
public interface UserRepository extends CrudRepository<User, Integer> {
}
