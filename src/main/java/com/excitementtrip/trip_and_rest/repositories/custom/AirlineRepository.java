package com.excitementtrip.trip_and_rest.repositories.custom;

import com.excitementtrip.trip_and_rest.entities.Airline;

import java.util.List;

public interface AirlineRepository {
    List<Airline> getByAirlineName(String ... airlineName);

    Airline getByAirlineName(String airlineName);

}
