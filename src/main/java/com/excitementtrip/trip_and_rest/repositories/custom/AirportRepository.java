package com.excitementtrip.trip_and_rest.repositories.custom;

import com.excitementtrip.trip_and_rest.entities.Airport;

import java.util.List;

public interface AirportRepository {

    List<Airport> getByAirportName(String ... airportName);
}
