package com.excitementtrip.trip_and_rest.repositories.custom;

import com.excitementtrip.trip_and_rest.entities.City;
import java.util.List;

public interface CityRepository{

    List<City> getByCityName(String... cityName);

    City getByCityName(String cityName);

}
