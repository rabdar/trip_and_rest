package com.excitementtrip.trip_and_rest.repositories.custom;

import com.excitementtrip.trip_and_rest.entities.Country;

public interface CountryRepository {

    Country getByCountryName(String countryName);
}
