package com.excitementtrip.trip_and_rest.repositories.custom;

import com.excitementtrip.trip_and_rest.entities.Hotel;

import java.util.List;

public interface HotelRepository {

    List<Hotel> getByHotelName(String ... hotelsNames);

    Hotel getByHotelName(String hotelName);
}

