package com.excitementtrip.trip_and_rest.repositories.custom.imp;

import com.excitementtrip.trip_and_rest.entities.Airline;
import com.excitementtrip.trip_and_rest.repositories.custom.AirlineRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class AirlineRepositoryImp implements AirlineRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List getByAirlineName(String... airlineNames) {

        Query query = entityManager.createQuery("select a from Airline as a where a.airlineName = :airlineNames");

        for (String airlineName: airlineNames){
            query.setParameter("airportName", airlineName);
        }

        return query.getResultList();
    }

    @Override
    public Airline getByAirlineName(String airlineName) {
        Query query = entityManager.createQuery("select a from Airline as a where a.airline.airlineName = :airlineName");

        query.setParameter("airlineName", airlineName);

        return (Airline) query.getSingleResult();
    }

}
