package com.excitementtrip.trip_and_rest.repositories.custom.imp;

import com.excitementtrip.trip_and_rest.entities.Airport;
import com.excitementtrip.trip_and_rest.repositories.custom.AirportRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class AirportRepositoryImp implements AirportRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Airport> getByAirportName(String... airportsNames) {

        Query query = entityManager.createQuery("select a from Airport as a where a.airport.airportName = :airportName");

        for (String airportName: airportsNames){
            query.setParameter("airportName", airportName);
        }

        return query.getResultList();
    }
}
