package com.excitementtrip.trip_and_rest.repositories.custom.imp;

import com.excitementtrip.trip_and_rest.entities.City;
import com.excitementtrip.trip_and_rest.repositories.custom.CityRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class CityRepositoryImp implements CityRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<City> getByCityName(String ... cityName1) {

        Query query = entityManager.createQuery("select c from City as c where c.cityName = :cityName");

        for (String cityName : cityName1) {
            query.setParameter("cityName", cityName);;
        }

        return query.getResultList();
    }

    @Override
    public City getByCityName(String cityName) {
        Query query = entityManager.createQuery("select c from City as c where c.cityName = :cityName");

        query.setParameter("cityName", cityName);;

        return (City) query.getSingleResult();
    }
}
