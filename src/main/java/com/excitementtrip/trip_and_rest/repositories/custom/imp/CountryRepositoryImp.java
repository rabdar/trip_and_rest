package com.excitementtrip.trip_and_rest.repositories.custom.imp;

import com.excitementtrip.trip_and_rest.entities.Country;
import com.excitementtrip.trip_and_rest.repositories.custom.CountryRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class CountryRepositoryImp implements CountryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Country getByCountryName(String countryName) {

        Query query = entityManager.createQuery("select c from Country as c where c.countryName = :countryName");
        query.setParameter("countryName", countryName);

        return (Country) query.getSingleResult();
    }
}
