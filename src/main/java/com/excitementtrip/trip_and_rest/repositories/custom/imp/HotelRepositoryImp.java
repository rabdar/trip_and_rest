package com.excitementtrip.trip_and_rest.repositories.custom.imp;

import com.excitementtrip.trip_and_rest.entities.Airline;
import com.excitementtrip.trip_and_rest.entities.Hotel;
import com.excitementtrip.trip_and_rest.repositories.custom.HotelRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class HotelRepositoryImp implements HotelRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Hotel> getByHotelName(String... hotelsNames) {

        Query query = entityManager.createQuery("select h from Hotel as h where h.hotel.hotelName = :hotelName");
        for (String hotelName : hotelsNames) {
            query.setParameter("hotelName", hotelName);;
        }

        return query.getResultList();
    }

    @Override
    public Hotel getByHotelName(String hotelName) {
        Query query = entityManager.createQuery("select h from Hotel as h where h.hotel.hotelName = :hotelName");

        query.setParameter("hotelName", hotelName);;

        return (Hotel) query.getSingleResult();
    }
}
