package com.excitementtrip.trip_and_rest.testData;

import com.excitementtrip.trip_and_rest.entities.*;
import com.excitementtrip.trip_and_rest.repositories.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.excitementtrip.trip_and_rest.entities.HotelBoard.*;

@Component
public class TestData {

    private final ExtendedAirlineRepository extendedAirlineRepository;
    private final ExtendedAirportRepository extendedAirportRepository;
    private final ExtendedCityRepository extendedCityRepository;
    private final ExtendedCountryRepository extendedCountryRepository;
    private final ExtendedHotelRepository extendedHotelRepository;
    private final ExtendedTourRepository extendedTourRepository;


    public TestData(ExtendedAirlineRepository extendedAirlineRepository, ExtendedAirportRepository extendedAirportRepository,
                    ExtendedCityRepository extendedCityRepository, ExtendedCountryRepository extendedCountryRepository,
                    ExtendedHotelRepository extendedHotelRepository, ExtendedTourRepository extendedTourRepository) {
        this.extendedAirlineRepository = extendedAirlineRepository;
        this.extendedAirportRepository = extendedAirportRepository;
        this.extendedCityRepository = extendedCityRepository;
        this.extendedCountryRepository = extendedCountryRepository;
        this.extendedHotelRepository = extendedHotelRepository;
        this.extendedTourRepository = extendedTourRepository;
    }

    public void addTestData() {

        Country country1 = new Country("Polska", Continent.EUROPE);
        Country country2 = new Country("USA", Continent.NORTH_AMERICA);
        Country country3 = new Country("Chiny", Continent.ASIA);

        List<Country> countryList = new ArrayList<>();
        countryList.add(country1);
        countryList.add(country2);
        countryList.add(country3);
        extendedCountryRepository.saveAll(countryList);

        City city1 = new City("Warszawa");
        City city2 = new City("New York");
        City city3 = new City("Pekin");

        List<City> cityList = new ArrayList<>();
        cityList.add(city1);
        cityList.add(city2);
        cityList.add(city3);
        extendedCityRepository.saveAll(cityList);

        Hotel hotel1 = new Hotel("Atlas", 4, "Bardzo daleko");
        Hotel hotel2 = new Hotel("Tamiza", 3, "Bardzo blisko");
        Hotel hotel3 = new Hotel("Sajgonki", 5, "Bardzo średnio");

        List<Hotel> hotelList = new ArrayList<>();
        hotelList.add(hotel1);
        hotelList.add(hotel2);
        hotelList.add(hotel3);
        extendedHotelRepository.saveAll(hotelList);

        Airport airport1 = new Airport("Modlin", "EG12657");
        Airport airport2 = new Airport("Kennedy", "EG12456");
        Airport airport3 = new Airport("Capital", "EG122346");

        List<Airport> airportList = new ArrayList<>();
        airportList.add(airport1);
        airportList.add(airport2);
        airportList.add(airport3);
        extendedAirportRepository.saveAll(airportList);


        country1.getCity().add(city1);
        country2.getCity().add(city2);
        country3.getCity().add(city3);
        extendedCountryRepository.saveAll(countryList);

        city1.setCountry(country1);
        city1.getAirport().add(airport1);
        city1.getHotel().add(hotel1);
        city2.setCountry(country2);
        city2.getAirport().add(airport2);
        city2.getHotel().add(hotel2);
        city3.setCountry(country3);
        city3.getAirport().add(airport3);
        city3.getHotel().add(hotel3);
        extendedCityRepository.saveAll(cityList);

        Airline airline1 = new Airline("Ryanair");
        Airline airline2 = new Airline("Wizzair");
        Airline airline3 = new Airline("LOT");

        List<Airline> airlineList = new ArrayList<>();
        airlineList.add(airline1);
        airlineList.add(airline2);
        airlineList.add(airline3);
        extendedAirlineRepository.saveAll(airlineList);

        Tour tour1 = new Tour("2021-02-12", "2021-02-18",
                5, AI, 1000.0, 800.0,
                true, 40, 25);

        Tour tour2 = new Tour("2022-02-12", "2022-02-20",
                5, BB, 1000.0, 800.0,
                true, 70, 50);

        Tour tour3 = new Tour("2022-03- 12", "2022-03-18",
                5, HB, 1000.0, 800.0,
                true, 40, 25
        );

        List<Tour> tourList = new ArrayList<>();
        tourList.add(tour1);
        tourList.add(tour2);
        tourList.add(tour3);
        extendedTourRepository.saveAll(tourList);

        hotel1.setCity(city1);
        hotel1.getTour().add(tour1);
        hotel2.setCity(city2);
        hotel2.getTour().add(tour2);
        hotel3.setCity(city3);
        hotel3.getTour().add(tour3);
        extendedHotelRepository.saveAll(hotelList);

        airport1.getCity().add(city1);
        airport1.getAirline().add(airline1);
        airport2.getCity().add(city2);
        airport2.getAirline().add(airline2);
        airport3.getCity().add(city3);
        airport3.getAirline().add(airline3);
        extendedAirportRepository.saveAll(airportList);

        airline1.getAirport().add(airport1);
        airline1.getTour().add(tour1);
        airline2.getAirport().add(airport2);
        airline2.getTour().add(tour2);
        airline3.getAirport().add(airport3);
        airline3.getTour().add(tour3);
        extendedAirlineRepository.saveAll(airlineList);

        tour1.setAirline(airline1);
        tour1.setHotel(hotel1);
        tour2.setAirline(airline2);
        tour2.setHotel(hotel2);
        tour3.setAirline(airline3);
        tour3.setHotel(hotel3);
        extendedTourRepository.saveAll(tourList);
    }

}


