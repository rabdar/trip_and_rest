package com.excitementtrip.trip_and_rest.testData;

import com.excitementtrip.trip_and_rest.entities.User;
import com.excitementtrip.trip_and_rest.repositories.UserRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserTestData {

    @Resource(name = "userRepository")
    private UserRepository userRepository;

    public void addUserData() {
        User user1 = new User();
        user1.setUserName("Karol");
        user1.setEmail("niewiadomski@gmail.com");

        User user2 = new User();
        user2.setUserName("Ala");
        user2.setEmail("nowakowska@gmail.com");

        userRepository.save(user1);
        userRepository.save(user2);
    }
}
